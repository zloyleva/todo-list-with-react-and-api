import React, { Component } from 'react';

import './form-create-item.css';

export default class FormCreateItem extends Component {

    state = {
        label: ''
    };

    onLabelChange = (e) => {
        this.setState({
            label: e.target.value
        })
    };

    onSubmit = (e) => {
        e.preventDefault();
        // const { label } = this.state;
        // this.setState({ label: '' });
        // const cb = this.props.onItemAdded || (() => { });
        // cb(label);
    };

    render() {
        return (
            <form
                className=""
                onSubmit={this.onSubmit}>

                <input type="text"
                    /*className=""
                    value={this.state.label}
                    onChange={this.onLabelChange}
                    placeholder="Create new task"*/ />

                <button type="submit"
                    className="">Add</button>
            </form>
        );
    }
}
