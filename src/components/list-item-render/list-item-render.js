import React from 'react';

import './list-item-render.css'

const ListItemRender = ({id, title, onDelete}) => {

        return(
            <li id={id} className="list__item" >
                <span>{title}</span>

                <button onClick={onDelete}>Delete</button>
                <button>Edit</button>
            </li>
        )
}

export default ListItemRender;