import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './todo-list.css';

import FormCreateItem from '../form-create-item';
import ListItemRender from '../list-item-render'
//import GetTasksApi from '../get-tasks-api'
//import ItemStatusFilter from './components/task-edit'
//import ItemStatusFilter from './components/task-form'
//import ItemStatusFilter from './components/task-render'

let url = `https://jsonplaceholder.typicode.com/todos/`;

export default class TodoList extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            
        };
    }

    async componentDidMount() {
        let response = await fetch(`${url}`);
        let data = await response.json();
            if (!response.ok) {
                throw new Error(`Could not fetch ${url}
                received ${response.status}`);
            }
        return data = this.setState({ obj: data.obj });
    }

    render(){
        const { obj } = this.state;

        return(
        <div className = "todos" >
                <FormCreateItem />
                <ul>
                    {obj.map(el =>
                        <ListItemRender id={el.id} title={el.title}/>
                    )}
                </ul>       
        </div>
        );
    }
}

