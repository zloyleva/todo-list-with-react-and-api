import React from 'react';
import './get-tasks-api.css'

let url = `https://jsonplaceholder.typicode.com/todos/`;

async function GetTasksApi(id) {
    let response = await fetch(`${url}${id}`);
    let data = await response.json();
    if (!response.ok) {
        throw new Error(`Could not fetch ${url}
                ${ id} received ${response.status}`);
    }
    return data;
}

export default GetTasksApi;