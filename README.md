<h1>About this project</h1>
Hi! 
This app downloads 3 random tasks using api https://jsonplaceholder.typicode.com/todos/. After downloading, you can add, edit and delete tasks. The application also handles the processing of data loading and errors.   

<h2>To deploy an application locally, you can:</h2>

### `git clone https://gitlab.com/andrew_florida/todo-list-with-react-and-api.git`
### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

<h2>File structure:</h2>
<pre class="code highlight js-syntax-highlight plaintext white" lang="plaintext" v-pre="true"><code>
├── public/                      # Build (autogenerate)
│   ├── index.html             # styles
├── src/                        # project scripts and files
│   ├── components/             
│      ├── get-tasks-api/              
│      ├── get-tasks-on-error/           
│      ├── get-tasks-onload/             
│      ├── task-delete/              
│      ├── task-edit/             
│      ├── task-form/           
│      ├── task-render/                         
|   ├── index.js/           
├── .gitignore           # files that are not added to the remote repository
├── package.json         # node modules configurations
<code></pre>

